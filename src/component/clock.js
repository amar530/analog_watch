function renderDate(component) {
    var date=document.createElement('div')
        var d=new Date()
        var finalDate=d.toDateString()
        date.setAttribute('id','date')
        date.appendChild(document.createTextNode(finalDate))
    component.appendChild(date)
}
function renderClockNeedles(component) {
     var id=['hour_hand','minute_hand','second_hand']
     id.forEach((element,i) => {
        var hand=document.createElement('div')
         hand.setAttribute('id',element)
         component.appendChild(hand)
        });
    renderDate(component)    
}

function renderClock(component) {
   renderClockNeedles(component)
}
// this is layout where all like clock image clock needles image will render
function renderClockLayout(component) {
    var wrapper=document.createElement('div')
        wrapper.setAttribute('id','wrapper')
    renderClock(wrapper)
    component.appendChild(wrapper)    
}
